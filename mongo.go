package helper

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoDB struct {
	ID string
}

var Mongo MongoDB

func (m MongoDB) Find(id string, connection string, database string, collection string, Resp interface{}) error {

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(connection))

	defer client.Disconnect(context.TODO())

	if err != nil {
		log.Println(err)
	}
	coll := client.Database(database).Collection(collection)

	idp, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Println(err)
		return err
	}
	filter := bson.M{"_id": idp}
	err = coll.FindOne(ctx, filter).Decode(Resp)
	if err != nil {
		//log.Println("Mongo decode error", err)
	}
	return err
}

func (m MongoDB) FindBy(filter interface{}, connection string, database string, collection string, Resp interface{}) error {

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(connection))

	defer client.Disconnect(context.TODO())

	if err != nil {
		log.Println(err)
	}
	coll := client.Database(database).Collection(collection)

	err = coll.FindOne(ctx, filter).Decode(Resp)
	if err != nil {
		log.Println(err)
	}
	return err
}

func (m MongoDB) Count(filter interface{}, connection string, database string, collection string) (int64, error) {

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(connection))

	defer client.Disconnect(context.TODO())

	if err != nil {
		log.Println(err)
	}
	coll := client.Database(database).Collection(collection)

	count, err := coll.CountDocuments(ctx, filter)
	if err != nil {
		log.Println(err)
	}
	return count, err
}

func (m MongoDB) DeleteMany(filter interface{}, connection string, database string, collection string) (*mongo.DeleteResult, error) {

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(connection))

	defer client.Disconnect(context.TODO())

	if err != nil {
		log.Println(err)
	}
	coll := client.Database(database).Collection(collection)

	if err != nil {
		log.Println(err)
	}

	deleteResult, err := coll.DeleteMany(ctx, filter)
	if err != nil {
		log.Println(err)
	}
	return deleteResult, err
}

func (m MongoDB) DeleteOne(filter interface{}, connection string, database string, collection string) (*mongo.DeleteResult, error) {

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(connection))

	defer client.Disconnect(context.TODO())

	if err != nil {
		log.Println(err)
	}
	coll := client.Database(database).Collection(collection)

	if err != nil {
		log.Println(err)
	}

	deleteResult, err := coll.DeleteOne(ctx, filter)
	if err != nil {
		log.Println(err)
	}
	return deleteResult, err
}

func (m MongoDB) Update(id string, updateData interface{}, connection string, database string, collection string) (*mongo.UpdateResult, error) {

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(connection))

	defer client.Disconnect(context.TODO())

	if err != nil {
		log.Println(err)
	}
	coll := client.Database(database).Collection(collection)

	idp, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Println(err)
	}
	filter := bson.M{"_id": idp}
	updateResult, err := coll.UpdateOne(ctx, filter, updateData)
	if err != nil {
		log.Println(err)
	}
	return updateResult, err
}

func (m MongoDB) Create(payload interface{}, connection string, database string, collection string) (id string, err error) {

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(connection))

	defer client.Disconnect(context.TODO())

	if err != nil {
		// log.Println(err)
		return
	}
	coll := client.Database(database).Collection(collection)

	insertResult, err := coll.InsertOne(ctx, payload)
	if err != nil {
		//log.Println(err)
		return
	}
	idp := insertResult.InsertedID.(primitive.ObjectID)
	id = idp.Hex()
	return
}

func (m MongoDB) List(connection string, database string, collection string, filter interface{}, callObj func(cur *mongo.Cursor), page int, pageSize int) error {
	ctx, _ := context.WithTimeout(context.Background(), 90*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(connection))

	defer client.Disconnect(context.TODO())

	if err != nil {
		return err
	}
	coll := client.Database(database).Collection(collection)
	cur, err := coll.Find(ctx, filter)

	if err != nil {
		return err
	}
	defer cur.Close(context.TODO())
	var itemSize int

	if page > 0 {
		skip := (page - 1) * pageSize
		for i := 1; i <= skip; i++ {
			cur.Next(ctx)
		}
	}
	for cur.Next(context.TODO()) {
		if itemSize >= pageSize {
			break
		}
		callObj(cur)

		if err != nil {
			log.Println(err)
		}
		itemSize++
	}

	return nil

}

func (m MongoDB) ListWithFilter(connection string, database string, collection string, filter interface{}, callObj func(cur *mongo.Cursor), page int64, pageSize int64, option *options.FindOptions) error {
	ctx, _ := context.WithTimeout(context.Background(), 90*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(connection))

	defer client.Disconnect(context.TODO())

	if err != nil {
		return err
	}
	var skip int64
	if page > 0 {
		skip = (page - 1) * pageSize
	}
	option.SetSkip(skip)
	option.SetLimit(pageSize)
	coll := client.Database(database).Collection(collection)
	cur, err := coll.Find(ctx, filter, option)

	if err != nil {
		return err
	}
	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()) {
		callObj(cur)
	}

	return nil

}
